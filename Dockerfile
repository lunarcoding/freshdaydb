FROM python:3.10-alpine AS build-python
RUN apk update && apk add --virtual build-essential gcc python3-dev musl-dev postgresql-dev jpeg-dev zlib-dev libffi-dev openssl-dev
RUN python -m venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH"
COPY ./requirements.txt .
RUN /opt/venv/bin/python -m pip install --upgrade pip
RUN pip install -r requirements.txt

FROM python:3.10-alpine
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV DEBUG 1
ENV PATH="/opt/venv/bin:$PATH"
COPY --from=build-python /opt/venv /opt/venv
RUN apk update && apk add --virtual build-deps gcc python3-dev musl-dev postgresql-dev jpeg-dev zlib-dev libffi-dev openssl-dev
RUN pip install psycopg2-binary
WORKDIR /app
COPY . .
RUN adduser -D myuser
USER myuser
CMD gunicorn freshdaydb.wsgi:application --bind 0.0.0.0:$PORT