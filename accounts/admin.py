from django.contrib import admin

# Register your models here.
from .models import *

# Head
# User
# Location
# Discount
# Stock
# ProductCategory
# Coupon
# UserPayment
# OrderPayment
# BasketSession
# Shop
# Order
# Product
# BasketItem
# OrderDetail

admin.site.register(Alluser)
admin.site.register(Location)
admin.site.register(Discount)
# admin.site.register(Stock)
# admin.site.register(ProductCategory)
admin.site.register(Coupon)
admin.site.register(UserPayment)
admin.site.register(OrderPayment)
admin.site.register(BasketSession)
# admin.site.register(Shop)
admin.site.register(Order)
admin.site.register(Product)
admin.site.register(BasketItem)
admin.site.register(OrderDetail)

