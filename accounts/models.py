from asyncio.windows_events import NULL
from itertools import product
from operator import mod
from pydoc import describe
from re import T
from unicodedata import category
from django.db import models
from django.contrib.auth.models import User
from django.forms import ChoiceField

# Head
# Alluser
# Location
# Discount
# Stock
# ProductCategory
# Coupon
# UserPayment
# OrderPayment
# BasketSession
# Shop
# Order
# Product
# BasketItem
# OrderDetail

# Create your models here.
class Alluser(models.Model):
    user = models.OneToOneField(User, null=True,blank = True, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=200, null=True)
    sir_name = models.CharField(max_length=200, null=True)
    phone_number = models.CharField(max_length=200, null=True)
    email = models.CharField(max_length=200, null=True)
    # profile_pic = models.ImageField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return self.first_name

class Location(models.Model):
    alluser_id = models.ForeignKey(
        Alluser, null=True, on_delete=models.SET_NULL)
    landmark = models.CharField(max_length=200,null=True)
    address_number = models.CharField(max_length=200,null=True)
    sub_district = models.CharField(max_length=200,null=True)
    district = models.CharField(max_length=200,null=True)
    city = models.CharField(max_length=200,null=True)
    postal = models.IntegerField(null=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)
    
    def __str__(self):
        return self.landmark

class Discount(models.Model):
    name = models.CharField(max_length=200, null=True, blank=True)
    description = models.CharField(max_length=200, null=True, blank=True)
    discount_percent = models.FloatField(null=True)
    activate = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)
    
    def __str__(self):
        return str(self.id)

# class Stock(models.Model):
#     quanity = models.IntegerField(null=True)
#     created_at = models.DateTimeField(auto_now_add=True, null=True)
#     updated_at = models.DateTimeField(auto_now=True, null=True)
    
#     def __str__(self):
#         return str(self.id)

# class ProductCategory(models.Model):
#     category = [('vegetableandfruit', 'vegetableandfruit'),
#               ('producvegetableandfruit', 'producvegetableandfruit'),
#               ('foodanddrink', 'foodanddrink'),
#               ]
#     name = models.CharField(max_length=200, null=True)
#     created_at = models.DateTimeField(auto_now_add=True, null=True)
#     updated_at = models.DateTimeField(auto_now=True, null=True)
#     product_category = models.CharField(max_length=200, null=True, choices=category)

#     def __str__(self):
#         return self.name

class Coupon(models.Model):
    alluser_id = models.ForeignKey(
        Alluser, null=True, on_delete=models.SET_NULL)
    name = models.CharField(max_length=200, null=True)
    description = models.CharField(max_length=200, null=True)
    discount = models.FloatField(null=True)
    expiration_date = models.DateTimeField(null=False)  
    activate = models.BooleanField(default=False)
    date_created = models.DateTimeField(auto_now_add=True, null=True)  
    updated_at = models.DateTimeField(auto_now=True, null=True) 
    def __str__(self):
        return self.name

class UserPayment(models.Model):
    alluser_id = models.ForeignKey(
        Alluser, null=True, on_delete=models.SET_NULL)
    provider = models.CharField(max_length=200, null=True)
    card_number = models.CharField(max_length=200, null=True)
    expiration_date = models.DateTimeField(null=False)
    
    def __str__(self):
        return self.provider
    
class OrderPayment(models.Model):
    user_payment_id = models.ForeignKey(
        UserPayment, null=True, on_delete=models.SET_NULL)
    status = models.CharField(max_length=200, null=True)
    date_created = models.DateTimeField(auto_now_add=True, null=True)  
    updated_at = models.DateTimeField(auto_now=True, null=True) 
    
    def __str__(self):
        return str(self.id)
    
class BasketSession(models.Model):
    alluser_id = models.ForeignKey(
        Alluser, null=True, on_delete=models.SET_NULL)
    total = models.FloatField(null=True)
    date_created = models.DateTimeField(auto_now_add=True, null=True)  
    updated_at = models.DateTimeField(auto_now=True, null=True) 
    
    def __str__(self):
        return str(self.id)
    

    
class Order(models.Model):
    status = [('waiting', 'waiting'),
              ('shipping', 'shipping'),
              ('success', 'success'),
              ('cancel', 'cancel'),
              ]
    alluser_id = models.ForeignKey(
        Alluser, null=True, on_delete=models.SET_NULL)
    amount = models.IntegerField(null=True)
    order_payment_id = models.ForeignKey(
        OrderPayment, null=True, on_delete=models.SET_NULL)
    coupon_id = models.ForeignKey(
        Coupon, null=True, on_delete=models.SET_NULL)
    location_id = models.ForeignKey(
        Location, null=True, on_delete=models.SET_NULL)

    date_created = models.DateTimeField(auto_now_add=True, null=True)  
    updated_at = models.DateTimeField(auto_now=True, null=True) 
    order_status = models.CharField(max_length=200, null=True, choices=status)
    def __str__(self):
        return str(self.id)


class Product(models.Model):
    category = [('vegetableandfruit', 'vegetableandfruit'),
              ('productvegetableandfruit', 'productvegetableandfruit'),
              ('foodanddrink', 'foodanddrink'),
              ]
    name = models.CharField(max_length=200, null=True)
    description = models.CharField(max_length=200, null=True, blank=True)
    price = models.FloatField(null=True)
    shop_id = models.ForeignKey(
        Alluser, null=True, on_delete=models.SET_NULL)
    discount_id = models.ForeignKey(
        Discount, null=True , on_delete=models.SET_NULL)
    # stock_id = models.ForeignKey(
    #     Stock, null=True, on_delete=models.SET_NULL)
    # product_category_id = models.ManyToManyField(ProductCategory)
    product_image = models.ImageField(null=True, blank=True)
    quanity = models.IntegerField(null=True)
    type = models.CharField(max_length=200, null=True, choices=category)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return self.name
    
class BasketItem(models.Model):
    basket_session_id = models.ForeignKey(
        BasketSession, null=True, on_delete=models.SET_NULL)
    product_id = models.ForeignKey(
        Product, null=True, on_delete=models.SET_NULL)
    quanity = models.IntegerField(null=True)
    date_created = models.DateTimeField(auto_now_add=True, null=True)  
    updated_at = models.DateTimeField(auto_now=True, null=True) 
    
    def __str__(self):
        return str(self.id)
    
class OrderDetail(models.Model):
    order_id = models.ForeignKey(
        Order, null=True, on_delete=models.SET_NULL)
    product_id = models.ForeignKey(
        Product, null=True, on_delete=models.SET_NULL)
    price = models.FloatField(null=True)
    quanity = models.IntegerField(null=True)
    date_created = models.DateTimeField(auto_now_add=True, null=True)  
    updated_at = models.DateTimeField(auto_now=True, null=True) 
    
    def __str__(self):
        return str(self.id)


# class CreateItem(models.Model):
#     pass