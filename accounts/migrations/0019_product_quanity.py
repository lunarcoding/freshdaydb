# Generated by Django 4.0 on 2022-03-05 10:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0018_shop_email_shop_phone_number'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='quanity',
            field=models.IntegerField(null=True),
        ),
    ]
