# Generated by Django 4.0 on 2022-03-05 14:28

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0022_rename_customer_alluser_delete_shop_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='product',
            name='shop_id',
        ),
    ]