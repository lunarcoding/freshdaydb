# Generated by Django 4.0 on 2022-03-05 14:45

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0025_coupon_discount_location_order_userpayment_product_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='alluser',
            name='user',
        ),
    ]
