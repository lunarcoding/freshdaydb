from django.db.models.signals import post_save
from .models import Alluser
from django.contrib.auth.models import Group

def customer_profile(sender, instance, created, **kwargs):
    if created:
        
        group = Group.objects.get(name='shop')
        instance.groups.add(group)
        Alluser.objects.create(
             user = instance
        )

post_save.connect(customer_profile, sender=Alluser)

