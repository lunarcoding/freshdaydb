from django.db.models import Q
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login , logout

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Group

# Create your views here.
from .models import *
from .forms import CreateUserForm, ItemForm, OrderForm
from .decorators import unauthenticated_user,allowed_users



@unauthenticated_user
def loginpage(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request,user)
            return redirect('home')
        else:
            messages.info(request, 'Username OR Password is incorrect')

    context = {}
    return render(request, 'accounts/login.html',context)

def logoutUser(request):
    logout(request)
    return redirect('login')

@unauthenticated_user
def register(request):
    form = CreateUserForm()
    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            username = form.cleaned_data.get('username')
            group = Group.objects.get(name='shop')
            user.groups.add(group)
            Alluser.objects.create(
                user = user
            )
            messages.success(request,'Account was created for ' + username)
            return redirect('login')
    
    context = {'form':form}
    return render(request, 'accounts/register2.html', context)

@login_required(login_url='login')
@allowed_users(allowed_roles=['shop','admin'])
def home(request):
    orders = Order.objects.all()
    orders_detail = OrderDetail.objects.all()
    best_seller = Product.objects.first()
    
    # customers = Alluser.objects.all()
    products = Product.objects.all()
    
    context = {'orders':orders,'orders_detail':orders_detail,'products':products, 'best_seller':best_seller}
    return render(request, 'accounts/dashboard.html', context)


# order
@login_required(login_url='login')
@allowed_users(allowed_roles=['shop','admin'])
def order(request):
    orders = Order.objects.all()
    orders_detail = OrderDetail.objects.all()
    context = {'orders':orders,'orders_detail':orders_detail,}
    return render(request, 'accounts/order.html', context)

@login_required(login_url='login')
@allowed_users(allowed_roles=['shop','admin'])
def waiting(request):    
    orders = Order.objects.filter(order_status = 'waiting')
    orders_detail = OrderDetail.objects.all()
    context = {'orders':orders,
               'orders_detail':orders_detail,}
    return render(request, 'accounts/order_waiting.html', context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['shop','admin'])
def shipping(request):
    orders = Order.objects.filter(order_status = 'shipping')
    orders_detail = OrderDetail.objects.all()
    context = {'orders':orders,'orders_detail':orders_detail}
    return render(request, 'accounts/order_shipping.html', context)

@login_required(login_url='login')
@allowed_users(allowed_roles=['shop','admin'])
def successandcancel(request):
    orders = Order.objects.filter(Q(order_status='success') | Q(order_status='cancel'))
    orders_detail = OrderDetail.objects.all()
    context = {'orders':orders,'orders_detail':orders_detail}
    return render(request, 'accounts/order_successandcancel.html', context)

# item
@login_required(login_url='login')
@allowed_users(allowed_roles=['shop','admin'])
def item(request):
    # formcreate
    form = ItemForm()
    if request.method == 'POST':
        form = ItemForm(request.POST,request.FILES)
        if form.is_valid():
            form.save()

            return redirect('/item')
        else:
            messages.info(request, 'Failed')
    # formcreate
    # formupdate
    products = Product.objects.all()
    discount = Discount.objects.all()
    context = {'products':products,'form':form, 'discount': discount}
    return render(request, 'accounts/item.html', context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['shop','admin'])
def vegetableandfruit(request):
    products = Product.objects.filter(type = 'vegetableandfruit')
    context = {'products':products}
    return render(request, 'accounts/item_vegetableandfruit.html', context)

@login_required(login_url='login')
@allowed_users(allowed_roles=['shop','admin'])
def productvegetableandfruit(request):
    products = Product.objects.filter(type = 'productvegetableandfruit')
    context = {'products':products}
    return render(request, 'accounts/item_producvegetableandfruit.html', context)

@login_required(login_url='login')
@allowed_users(allowed_roles=['shop','admin'])
def foodanddrink(request):
    products = Product.objects.filter(type = 'foodanddrink')
    context = {'products':products}
    # if types != checktype[2]:
    #     return render(request, 'accounts/item_foodanddrink.html', NULL)
    
    return render(request, 'accounts/item_foodanddrink.html', context)


# @login_required(login_url='login')
# @allowed_users(allowed_roles=['shop','admin'])
# def create(request):
#     form = ItemForm()
#     if request.method == 'POST':
#         form = ItemForm(request.POST)
#         if form.is_valid():
#             form.save()
#             return redirect('/')
#         else:
#             messages.info(request, 'Failed')
#     context = {'form':form}
#     return render(request, 'accounts/create_item.html', context)

@login_required(login_url='login')
@allowed_users(allowed_roles=['shop','admin'])
def update(request,pk):
    if request.method == "POST":
        name = request.POST['name']
        description = request.POST['description']
        price = request.POST['price']
        discount_id = request.POST['discount_id']
        product_image = request.FILES['product_image']
        quanity = request.POST['quanity']
        type = request.POST['type']
        if(name==''or price==''or type==''):
            messages.warning(request,"Please fill form Correctly..!")
        else:
            emp = Product(name=name, description=description, price=price,discount_id=discount_id,product_image=product_image,quanity=quanity,type=type,id=id)
            emp.save()
            messages.success(request,"Data updated Successfully..!")
    return redirect('/item')

@login_required(login_url='login')
@allowed_users(allowed_roles=['shop','admin'])
def update_i(request,pk):
    item = Product.objects.get(id=pk)
    form = ItemForm(instance=item)
    if request.method == 'POST':
        form = ItemForm(request.POST, instance=item)
        if form.is_valid():
            form.save()
            return redirect('/')
        else:
            messages.info(request, 'Failed')
    context = {'form':form}
    return render(request, 'accounts/update_next.html', context)

@login_required(login_url='login')
@allowed_users(allowed_roles=['shop','admin'])
def delete(request, pk):
    item = Product.objects.get(id = pk)
    if request.method == "POST":
        item.delete()
        return redirect('/item')
    context = {'item',item}
    return render(request, 'accounts/delete_item.html',context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['shop','admin'])
def createorder(request):
    
    form = ItemForm()
    if request.method == 'POST':
        form = ItemForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/')
        else:
            messages.info(request, 'Failed')
    
    context = {'form':form}
    return render(request, 'accounts/order_waiting.html', context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['shop','admin'])
def receiveorder(request, pk):
    if request.method == "POST":
        Order.objects.filter(id = pk).update(order_status="shipping")
        return redirect('/waiting')
    
    return render(request, 'accounts/receive_order.html')


@login_required(login_url='login')
@allowed_users(allowed_roles=['shop','admin'])
def cancelorder(request, pk):
    if request.method == "POST":
        Order.objects.filter(id = pk).update(order_status="cancel")
        return redirect('/successandcancel')
    
    return render(request, 'accounts/cancel_order.html')

@login_required(login_url='login')
@allowed_users(allowed_roles=['shop','admin'])
def confirmorder(request, pk):
    if request.method == "POST":
        Order.objects.filter(id = pk).update(order_status="success")
        return redirect('/successandcancel')
    
    return render(request, 'accounts/shipping_order.html')

@login_required(login_url='login')
@allowed_users(allowed_roles=['shop','admin'])
def orderdetail(request,pk):
    order = Order.objects.get(id=pk)
    form = OrderForm()
    if request.method == 'POST':
        form = ItemForm(request.POST, instance=order)
        if form.is_valid():
            form.save
            
        
    context = {'form':form,}
    return render(request, 'accounts/order_detail.html', context)








# def index(request):
#     all_data = Product.objects.all()
#     employees = {'all_data':all_data}
#     return render(request,"accounts/item.html",employees)

# @login_required(login_url='login')
# @allowed_users(allowed_roles=['shop','admin'])
# def update_data(request,id):
#     if request.method == "POST":
#         name = request.POST['name']
#         description = request.POST['description']
#         price = request.POST['price']
#         shop_id = request.POST['shop_id']
#         stock_id = request.POST['stock_id']
#         product_image = request.FILES['product_image']
#         type = request.POST['type']
#         category = request.POST['category']
#         if(name==''or price==''or type==''):
#             messages.warning(request,"Please fill form Correctly..!")
#         else:
#             emp = Product(name=name, description=description, price=price, shop_id=shop_id,stock_id=stock_id,product_image=product_image,type=type,category=category,id=id)
#             emp.save()
#             messages.success(request,"Data updated Successfully..!")
#     return redirect('/item')