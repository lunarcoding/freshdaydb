from asyncio.windows_events import NULL
from itertools import product
from operator import mod
from pydoc import describe
from re import T
import string
from unicodedata import name
from django.db import models
from django.contrib.auth.models import User

from accounts.models import Customer, Location, Discount, Stock, ProductCategory, Coupon, UserPayment, OrderPayment, BasketSession, Shop, Order, Product, BasketItem, OrderDetail


#***(1)Returns all customers from customer table
customers = Customer.objects.all()
first_customer = Customer.objects.first()
last_customer = Customer.objects.last()
customer_by_name = Customer.objects.get(name='Peter Piper')
customer_by_id = Customer.objects.get(id=4)
first_customer.order_set.all()

#(7)***Returns orders customer name: (Query parent model values)
order5 = Order.__doc__
first_order = Order.objects.first()
last_order = Order.objects.last()
order_by_name = Order.objects.get(Order.__name__)
order_by_id = Order.objects.get(Order.id)
parentName = first_order.customer.name
Order.__qualname__

#(8)***Returns products from products table with value of "Out Door" in category attribute
product_category = Product.objects.filter(category="Out Door")
products = Product.objects.all()

#(9)***Order/Sort Objects by id
leastToGreatest = Product.objects.all().order_by('id') 
greatestToLeast = Product.objects.all().order_by('-id') 


#(10) Returns all products with tag of "Sports": (Query Many to Many Fields)
productsFiltered = Product.objects.filter(tags__name="Sports")

'''
(11)Bonus
Q: If the customer has more than 1 ball, how would you reflect it in the database?
A: Because there are many different products and this value changes constantly you would most 
likly not want to store the value in the database but rather just make this a function we can run
each time we load the customers profile
'''

#Returns the total count for number of time a "Ball" was ordered by the first customer
ballOrders = first_customer.order_set.filter(product__name="Ball").count()

#Returns total count for each product orderd
allOrders = {}

for order in first_customer.order_set.all():
	if order.product.name in allOrders:
		allOrders[order.product.name] += 1
	else:
		allOrders[order.product.name] = 1

#Returns: allOrders: {'Ball': 2, 'BBQ Grill': 1}


#RELATED SET EXAMPLE
class ParentModel(models.Model):
	name = models.CharField(max_length=200, null=True)

class ChildModel(models.Model):
	parent = models.ForeignKey(Customer)
	name = models.CharField(max_length=200, null=True)

parent = ParentModel.objects.first()
#Returns all child models related to parent
parent.childmodel_set.all()