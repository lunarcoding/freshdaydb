from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

from .models import Order, Product

class OrderForm(ModelForm):
    class Meta:
       model = Order
       fields = '__all__'

class CreateUserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']
        
class ItemForm(ModelForm):
    class Meta:
       model = Product
       fields = '__all__'
