from django.urls import path
from . import views

urlpatterns = [
    
    path('', views.home, name="home"),
    path('login/', views.loginpage, name="login"),
    path('logout/', views.logoutUser, name="logout"),
    path('register/', views.register, name="register"),
    path('order/', views.order, name="order"),
    path('waiting/', views.waiting, name="waiting"),
    path('shipping/', views.shipping, name="shipping"),
    path('successandcancel/', views.successandcancel, name="successandcancel"),
    path('item/', views.item, name="item"),
    path('vegetableandfruit/', views.vegetableandfruit, name="vegetableandfruit"),
    path('productvegetableandfruit/', views.productvegetableandfruit, name="productvegetableandfruit"),
    path('foodanddrink/', views.foodanddrink, name="foodanddrink"),
    path('item/<str:pk>', views.update, name="update"),
    path('delete/<str:pk>', views.delete, name="delete"),
    path('receiveorder/<str:pk>', views.receiveorder, name="receiveorder"),
    path('cancelorder/<str:pk>', views.cancelorder, name="cancelorder"),
    path('confirmorder/<str:pk>', views.confirmorder, name="confirmorder"),
    path('orderdetail/<str:pk>', views.orderdetail, name="orderdetail"),
    
]