from asyncio import Task
from rest_framework import generics, permissions
from rest_framework.response import Response
from rest_framework.decorators import api_view
from knox.models import AuthToken
from accounts.models import *
from .serializers import *
from django.contrib.auth import login
from rest_framework.authtoken.serializers import AuthTokenSerializer
from knox.views import LoginView as KnoxLoginView

from api import serializers


@api_view(['GET'])
def getData(request):
    user = request.user
    if user.is_authenticated:
        items =  Alluser.objects.all()
        serializer = AlluserSerializers(items, many=True)
        return Response(serializer.data)
    return Response({'error': 'not authenticated'}, status=400)

# patterns
#  {
#  "id" : 1,
#  "first_name" : "khaikem",
#  "sir_name" : "chobkiwna",
#  "phone_number" : "0957043137",
#  "email" : "khaikem@gmail.com"
# }

@api_view(['POST'])
def addAlluser(request):
    serializer = AlluserSerializers(data=request.data)
    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)

# @api_view(['UPDATE'])
# def editAlluser(request):
#     serializer = AlluserSerializers(data=request.data)
#     if serializer.is_valid():
#         serializer.save()
#     return Response(serializer.data)


# @api_view(['DELETE'])
# def deleteAlluser(request):
#     serializer = AlluserSerializers(data=request.data)
#     if serializer.is_valid():
#         serializer.save()
#     return Response(serializer.data)

class RegisterAPI(generics.GenericAPIView):
    serializer_class = RegisterSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        return Response({
        "user": UserSerializer(user, context=self.get_serializer_context()).data,
        "token": AuthToken.objects.create(user)[1]
        })
        
class LoginAPI(KnoxLoginView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request, format=None):
        serializer = AuthTokenSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        login(request, user)
        return super(LoginAPI, self).post(request, format=None)

@api_view(['GET'])
def get_uesr_data(request):
    user = request.user

    if user.is_authenticated:
        return Response({
            'user_info' : {
                'id' : user.id,
            }
        })

    return Response({'error':'not authenticated'}, status=400)

@api_view(['GET'])
def onlyuser(request, pk):
    user = request.user
    if user.is_authenticated:
        user = Alluser.objects.get(user=pk)
        serializer = AlluserSerializers(user, many=False)
        return Response(serializer.data)
    return Response({'error':'not authenticated'}, status=400)

@api_view(['POST'])
def createitem(request):
    serializer = ProductSerializers(data=request.data)
    
    if serializer.is_valid():
        serializer.save()
        
    return Response(serializer.data)

@api_view(['GET'])
def item(request):
    items = Product.objects.all()
    serializer = ProductSerializers(items, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def order(request):
    orders = Order.objects.all().order_by('-id')
    serializer = OrderSerializers(orders, many=True)
    return Response(serializer.data)

@api_view(['POST'])
def createorder(request):
    serializer = OrderSerializers(data=request.data)
    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)

@api_view(['GET'])
def vegetableandfruit(request):
    items = Product.objects.filter(category='vegetableandfruit')
    serializer = ProductSerializers(items, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def productvegetableandfruit(request):
    items = Product.objects.filter(category='productvegetableandfruit')
    serializer = ProductSerializers(items, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def foodanddrink(request):
    items = Product.objects.filter(category='foodanddrink')
    serializer = ProductSerializers(items, many=True)
    return Response(serializer.data)