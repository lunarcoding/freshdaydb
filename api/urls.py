from urllib.parse import urlparse
from django.urls import path
from . import views
from .views import RegisterAPI, LoginAPI
from knox import views as knox_views

urlpatterns = [
    path('',views.getData),
    path('add/',views.addAlluser),
    path('register/', RegisterAPI.as_view(), name='registerapi'),
    path('login/', LoginAPI.as_view(), name='loginapi'),
    path('logout/', knox_views.LogoutView.as_view(), name='logoutapi'),
    path('logoutall/', knox_views.LogoutAllView.as_view(), name='logoutallapi'),
    path('user/', views.get_uesr_data, name='userapi'),
    path('user/<str:pk>/', views.onlyuser, name='onlyuserapi'),
    path('createitem/', views.createitem, name='Productaddapi'),
    path('createorder/', views.createorder, name='Createorderapi'),
    path('item/', views.item, name='Productitemapi'),
    path('order/', views.order, name='orderapi'),
    path('foodanddrink/', views.foodanddrink, name='foodanddrinkapi'),
    path('productvegetableandfruit/', views.productvegetableandfruit, name='productvegetableandfruitapi'),
    path('vegetableandfruit/', views.productvegetableandfruit, name='vegetableandfruitapi'),
]
